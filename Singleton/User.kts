
object User{
    var name = "Israel"

    init {
        println("Singleton invoked")
    }

    fun printName() = println(name)
}

User.printName()
User.name="Jose"
User.printName()