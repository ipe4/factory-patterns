interface User {
    val name: String
    val surname: String
    fun getFullName() = "$name $surname"
    fun status(): String
    fun showAds(): Boolean
}



object UserFactory {
    fun getUser(userType: UserType, name: String, surname: String): User {
        return when (userType) {
            UserType.Normal -> Normal(name = name, surname = surname)
            UserType.Premium -> Premium(name = name, surname = surname)
        }
    }
}




enum class UserType { Normal, Premium }

class Normal(override val name: String, override val surname: String) : User {
    override fun status() = "Normal"
    override fun showAds() = true
}

class Premium(override val name: String, override val surname: String) : User {
    override fun status() = "Premium"
    override fun showAds() = false
}

fun main() {
    val normal = UserFactory.getUser(UserType.Normal, "James", "Smith")
    with(normal) {
        println(getFullName())
        println(status())
        println("Show ads: ${showAds()}")
    }



    val premium = UserFactory.getUser(UserType.Premium, "Peter", "Brown")
    with(premium) {
        println(getFullName())
        println(status())
        println("Show ads: ${showAds()}")
    }
}